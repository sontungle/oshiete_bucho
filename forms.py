# -*- coding: utf-8 -*-

"""
Define input forms for application
Using Flask-WTF
"""


# Import needed modules
from wtforms import Form, TextField, PasswordField, BooleanField, SelectField, TextAreaField, validators, ValidationError
from models import User

class RegistrationForm(Form):
    """
    Class for registration form to register new user
    """
    # Custom validation methods
    def nickname_not_exists_check(form, nickname):
        """
        Raise error if nickname already been registered
        """
        if User.nickname_exists(nickname.data):
            raise ValidationError('This nickname already exists. Choose another one')

    def email_not_exists_check(form, email):
        """
        Raise error if email already been registered
        """
        if User.email_exists(email.data):
            raise ValidationError('This email already exists. Choose another one')
    # Email Field
    email = TextField(u'メールアドレス', [
        validators.InputRequired(),
        validators.Email(message='Invalid email format'),
        email_not_exists_check
    ])
    # Nickname Field
    nickname = TextField(u'ニックネーム', [
        validators.InputRequired(),
        validators.Length(min=3, max=80, message='Nickname must be between 3 and 80 characters'),
        nickname_not_exists_check
    ])
    # Password Field
    password = PasswordField(u'パスワード', [
        validators.InputRequired(),
        validators.Length(min=5, max=80, message='Password must be between 5 and 80 characters'),
        validators.EqualTo('password_confirm', message='Password not matched')
    ])
    password_confirm = PasswordField(u'パスワード(確認用)')

class LoginForm(Form):
    """
    Class for login form to login user
    """
    # Email Field
    email = TextField(u'メールアドレス', [validators.InputRequired()])
    # Password Field
    password = PasswordField(u'パスワード', [validators.InputRequired()])
    # Remember Me
    remeber_me = BooleanField(u'30日間ログインする')

class RenewPasswordForm(Form):
    """
    Class for renew password form to publish new password for user
    """
    # Custom validation method
    def email_exists_check(form, email):
        """
        Raise error if iput email is not registered
        """
        if not User.email_exists(email.data):
            raise ValidationError(u'登録されてないアドレスです')
    # Email Field
    email = TextField(u'メールアドレス', [
        validators.InputRequired(),
        email_exists_check
    ])

class ContactForm(Form):
    """
    Class for contact form to let user contact admin
    """
    # Email Field
    email = TextField(u'メールアドレス', [
        validators.InputRequired(),
        validators.Email(message='Invalid email format')
    ])
    # Type Field
    question_type = SelectField(u'種別', default='2', choices=[
        ('1', u'報告掲載について'),
        ('2', u'buchoについて'),
        ('3', u'教えて!goo関係者の者ですが')
    ])
    # Content Field
    content = TextAreaField(u'内容', [validators.InputRequired()])

class ChangeInfoForm(Form):
    """
    class for change info form to let user change their current information
    """
    # Custom validation methods
    def nickname_not_exists_check(form, nickname):
        """
        Raise error if nickname already been registered
        """
        if User.nickname_exists(nickname.data):
            raise ValidationError('This nickname already exists. Choose another one')

    def email_not_exists_check(form, email):
        """
        Raise error if email already been registered
        """
        if User.email_exists(email.data):
            raise ValidationError('This email already exists. Choose another one')
    # Current password field
    current_password=PasswordField(u'現在のパスワード', [validators.InputRequired()])
    # Email Field
    email = TextField(u'メールアドレス', [
        validators.InputRequired(),
        validators.Email(message='Invalid email format'),
        email_not_exists_check
    ])
    # Nickname Field
    nickname = TextField(u'ニックネーム', [
        validators.InputRequired(),
        validators.Length(min=3, max=80, message='Nickname must be between 3 and 80 characters'),
        nickname_not_exists_check
    ])
    # Password Field
    password = PasswordField(u'パスワード', [
        validators.InputRequired(),
        validators.Length(min=5, max=80, message='Password must be between 5 and 80 characters'),
        validators.EqualTo('password_confirm', message='Password not matched')
    ])
    password_confirm = PasswordField(u'パスワード(確認用)')

class DeactiveForm(Form):
    """
    Class for deactive form to deactivate user
    """
    # Reason field
    reason = TextAreaField(u'退会理由', [validators.InputRequired()])

class QuestionForm(Form):
    """
    Class for question form to ask new question
    """
    # Content field
    content = TextAreaField(u'内容', [validators.InputRequired()])

class AnswerForm(Form):
    """
    Class for answer form to reply to a question
    """
    # Content field
    content = TextAreaField(u'回答', [validators.InputRequired()])
