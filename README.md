# Oshiete ! bucho by LE SON TUNG #

This is a demo trial web app written as a test from BeProud JP

### Environment ###

* Python 2.7.8
* Flask Framework
* Dependencies: Flask-SqlAlchemy, Flask-Login, Flask-WTF, sendgrid

### How to set up and running ###

* You need Mysql server and config the app to use the server in config.py
* We use sendgrid to send mail. Use can create free sendgrid account then config it in config.py
* Install all dependencies