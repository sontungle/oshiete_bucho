# -*- coding: utf-8 -*-

"""
The application file
Handle application logic and routing handlers
"""

# Import needed modules
import string
import random
import sendgrid
import datetime
from flask import Flask, render_template, redirect, url_for, request, session
from flask.ext.login import LoginManager, login_user, logout_user, current_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash
from forms import *
from models import *

# Create new application instance
app = Flask(__name__)
app.config.from_pyfile('config.py')

# Import and init the db instance
db.app = app
db.init_app(app)
db.create_all()

# Init sendgrid instance to send mail
sg = sendgrid.SendGridClient(app.config['SENDGRID_USERNAME'], app.config['SENDGRID_PASSWORD'])

# Init LoginManager instance to manage login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = '/login'

# User loader method used by flask-login
@login_manager.user_loader
def load_user(user_id):
    """
    This callback is used to reload the user object from the user ID stored in the session
    """
    return User.query.get(user_id)

# Index route
@app.route('/')
def index():
    # If user is log in redirect to main page
    if not current_user.is_anonymous():
        return redirect(url_for('main'))
    # If user is not login render the index page
    else:
        return render_template('index.html')

# Login route
@app.route('/login', methods=['GET', 'POST'])
def login():
    login_form = LoginForm(request.form)
    if request.method == 'POST':
        user = User.query.filter_by(email=login_form.email.data).first()
        # If the email and password matched, log the user in
        # Then, return user to the main page or continue
        if user and check_password_hash(user.password, login_form.password.data) and user.is_active():
            login_user(user, remember=login_form.remeber_me.data)
            return redirect(request.args.get('next') or url_for('main'))
        # Else render the login page with error
        else:
            return render_template('login.html', form=login_form, error = u'メールアドレスかパスワードが違うのためログインできません')
    # If GET request just render the login page
    return render_template('login.html', form=login_form)

# Logout route
@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/')

# Register route
@app.route('/register', methods=['GET'])
def register():
    # Just render the register form
    register_form = RegistrationForm(request.form)
    return render_template('register.html', form=register_form)

# Register confirm route
@app.route('/register_confirm', methods=['POST'])
def register_confirm():
    register_form = RegistrationForm(request.form)
    # If submit data is valid render the confirm page
    # Save needed information to session
    if register_form.validate():
        session['new_user_email'] = register_form.email.data
        session['new_user_nickname'] = register_form.nickname.data
        session['new_user_password'] = generate_password_hash(register_form.password.data)
        return render_template('register_confirm.html')
    # Else render the register form with errors
    else:
        return render_template('register.html', form=register_form)

# Register complete route
@app.route('/register_complete', methods=['POST'])
def register_complete():
    # Commit the db transaction to add new user
    # Then clear information in session
    new_user = User()
    new_user.email = session['new_user_email']
    new_user.nickname = session['new_user_nickname']
    new_user.password = session['new_user_password']
    new_user.active = False
    db.session.add(new_user)
    db.session.commit()
    session.pop('new_user_email', None)
    session.pop('new_user_nickname', None)
    session.pop('new_user_password', None)
    # Generate random activation code
    # Create new entry for activation table
    sample_pool = [str(x) for x in range(0,9)] + list(string.uppercase) + list(string.lowercase)
    activation_code = ''.join(random.sample(sample_pool, 16))
    new_activation = Activation.create_activation(new_user.email, activation_code)
    # Send mail with the link to activate new user
    activation_mail = sendgrid.Mail()
    activation_mail.add_to(new_user.email)
    activation_mail.set_subject('Activate your account')
    activation_mail.set_html('<a href="' + app.config['HOST_URL'] + '/activate/' + \
            str(new_user.email) + '/' + str(activation_code) +'">Active your account</a>')
    activation_mail.set_from(app.config['HOST_MAIL'])
    sg.send(activation_mail)
    # Render the complete page
    return render_template('register_complete.html')

# Activate user account
@app.route('/activate/<string:user_email>/<string:activation_code>')
def activate(user_email, activation_code):
    # Check if record exists
    activation = Activation.get_activation(user_email, activation_code)
    # If yes activate the user than delete the activation record
    # Then log the user in
    if activation:
        User.active_user_with_email(user_email)
        db.session.delete(activation)
        db.session.commit()
        user = User.query.filter_by(email=user_email).first()
        login_user(user)
        return redirect(url_for('main'))
    # Else tell the user the link is not valid
    else:
        message = 'The activation link is not valid'
        # Render the template with appropriate message
        return render_template('activate_fail.html', message = message)

# Change info
@app.route('/change_info', methods=['GET', 'POST'])
@login_required
def change_info():
    change_info_form = ChangeInfoForm(request.form)
    if request.method == 'POST' and change_info_form.validate() \
        and check_password_hash(current_user.password, change_info_form.current_password.data):
            current_user.email = change_info_form.email.data
            current_user.nickname = change_info_form.nickname.data
            current_user.password = generate_password_hash(change_info_form.password.data)
            db.session.commit()
            return render_template('change_info_complete.html')
    else:
        return render_template('change_info.html', form=change_info_form)

# Deactive account route
@app.route('/deactive', methods=['GET', 'POST'])
@login_required
def deactive():
    deactive_form = DeactiveForm(request.form)
    # If user input reason and submit
    # Make the user inactive
    # Then log the user out and render the deactive template
    # Save information to database
    if request.method == 'POST' and deactive_form.validate():
        current_user.active = False
        deactive_record = DeactiveRecord(current_user.email, deactive_form.reason.data, datetime.datetime.now())
        db.session.add(deactive_record)
        db.session.commit()
        logout_user()
        return render_template('deactive_complete.html')
    # Else render the tempate
    else:
        return render_template('deactive.html', form=deactive_form)

# Renew password route
@app.route('/renew_password', methods=['GET', 'POST'])
def renew_password():
    renew_password_form = RenewPasswordForm(request.form)
    # If form validate, random new password then send mail to user
    # Update new password to user
    if request.method == 'POST' and renew_password_form.validate():
        sample_pool = [str(x) for x in range(0,9)] + list(string.uppercase) + list(string.lowercase)
        new_password = ''.join(random.sample(sample_pool, 8))
        user = User.query.filter_by(email=renew_password_form.email.data).first()
        user.password = generate_password_hash(new_password)
        db.session.commit()
        password_mail = sendgrid.Mail()
        password_mail.add_to(user.email)
        password_mail.set_subject('New password')
        password_mail.set_text(new_password)
        password_mail.set_from(app.config['HOST_MAIL'])
        sg.send(password_mail)
        return render_template('renew_password_complete.html')
    # Else render the template
    else:
        return render_template('renew_password.html', form=renew_password_form)

# Contact route
@app.route('/contact', methods=['GET', 'POST'])
def contact():
    contact_form = ContactForm(request.form)
    # If valid form add new contact content record to database
    # Send notification mail to the user
    if request.method == 'POST' and contact_form.validate():
        contact_content = ContactContent(contact_form.email.data, contact_form.question_type.data, contact_form.content.data)
        db.session.add(contact_content)
        db.session.commit()
        contact_notify_mail = sendgrid.Mail()
        contact_notify_mail.add_to(contact_form.email.data)
        contact_notify_mail.set_subject('お問い合わせありがとうございました')
        contact_notify_mail.set_text('お問い合わせありがとうございました')
        contact_notify_mail.set_from(app.config['HOST_MAIL'])
        sg.send(contact_notify_mail)
        return render_template('contact_finish.html')
    return render_template('contact.html', form=contact_form)

# Main route
@app.route('/main')
@app.route('/main/<int:page>/<int:answer_page>')
@login_required
def main(page=1, answer_page=1):
    """
    Render the main page of user
    Have two parameters that indicate the page of current questions and current answers
    For pagination
    """
    info = {}
    # Get recent ask question
    recent_questions = current_user.questions.order_by(Question.time.desc()).paginate(page, app.config['ITEM_PER_PAGE'], False)
    info['recent_questions'] = recent_questions
    # Get recent answer question
    recent_answers = current_user.answers.order_by(Answer.time.desc()).paginate(answer_page, app.config['ITEM_PER_PAGE'], False)
    info['recent_answers'] = recent_answers
    return render_template('main.html', info=info)

# Recent question route
@app.route('/recent_questions')
@app.route('/recent_questions/<int:page>')
@login_required
def recent_question(page=1):
    questions = Question.query.order_by(Question.time.desc()).paginate(page, app.config['ITEM_PER_PAGE'] * 2, False)
    return render_template('filter_questions.html', category=u'最近された', questions=questions)

# Recent answered question route
@app.route('/unanswered_questions')
@app.route('/unanswered_questions/<int:page>')
@login_required
def unanswered_question(page=1):
    questions = Question.query.outerjoin(Question.answers).group_by(Question.id).having('count(answer.id) = 0').paginate(page, app.config['ITEM_PER_PAGE'] * 2, False)
    print questions.items
    return render_template('filter_questions.html', category=u'まだ未回答の', questions=questions)

# Ask Question route
@app.route('/new_question', methods=['GET', 'POST'])
@login_required
def new_question():
    question_form = QuestionForm(request.form)
    # If valid form submit add new question to database
    # Then redirect user to main page
    if request.method == 'POST' and question_form.validate():
        question = Question(current_user.id, question_form.content.data, datetime.datetime.now())
        db.session.add(question)
        db.session.commit()
        return redirect(url_for('question_detail', id=question.id))
    # Else render the template
    else:
        return render_template('new_question.html', form=question_form)

# Question detail route
@app.route('/question/<int:id>', methods=['GET', 'POST'])
@login_required
def question_detail(id):
    # Getting question and answer information
    info = {}
    question = Question.query.get_or_404(id)
    info['question'] = question
    answers = question.answers.order_by(Answer.time.desc()).all()
    info['answers'] = answers
    # Create the form
    answer_form = AnswerForm(request.form)
    # If valid form submit
    # Add new answer to database then refresh the page
    if request.method == 'POST' and answer_form.validate():
        answer = Answer(id, current_user.id, answer_form.content.data, datetime.datetime.now())
        db.session.add(answer)
        db.session.commit()
        return redirect(url_for('question_detail', id=id))
    # Else render the template
    # Only render the answer form if current user have not yet answered
    else:
        if Answer.already_answered(id, current_user.id):
            answer_form = None
        return render_template('question_detail.html', info=info, form=answer_form)

# Error handler
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500

# Run the application when run this script
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
