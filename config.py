"""
Config file for the application
"""

from datetime import timedelta

DEBUG = True

# General config
SECRET_KEY =
HOST_URL =
HOST_MAIL =
ITEM_PER_PAGE =

# Sendgrid config
SENDGRID_USERNAME =
SENDGRID_PASSWORD =

# Database config
SQLALCHEMY_DATABASE_URI =

# Flask-Login config
REMEMBER_COOKIE_DURATION =
