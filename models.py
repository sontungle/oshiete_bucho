# -*- coding: utf-8 -*-

"""
Define model objects for application
Using SQLAlchemy for Flask
"""

# Import needed modules
from flask.ext.sqlalchemy import SQLAlchemy

# Create new db instance
db = SQLAlchemy()

class User(db.Model):
    """
    Model for the User table
    """
    id = db.Column(db.Integer, primary_key = True)
    nickname = db.Column(db.String(80), unique = True)
    email = db.Column(db.String(80), unique = True)
    password = db.Column(db.String(128))
    active = db.Column(db.Boolean)
    questions = db.relationship('Question', backref='owner', lazy='dynamic')
    answers = db.relationship('Answer', backref='owner', lazy='dynamic')

    # Methods needed by Flask-Login module
    def is_authenticated(self):
        """
        Returns True if the user is authenticated, i.e. they have provided valid credentials.
        """
        return True

    def is_active(self):
        """
        Returns True if this is an active user
        Inactive accounts may not log in
        """
        return self.active

    def is_anonymous(self):
        """
        Returns True if this is an anonymous user. (Actual users should return False instead.)
        """
        return False

    def get_id(self):
        """
        Returns a unicode that uniquely identifies this user,
        and can be used to load the user from the user_loader callback.
        """
        return unicode(self.id)

    @staticmethod
    def nickname_exists(nickname):
        """
        Return True if an nickname exists in database
        """
        user = User.query.filter_by(nickname=nickname).first()
        return user is not None

    @staticmethod
    def email_exists(email):
        """
        Return True if an email exists in database
        """
        user = User.query.filter_by(email=email).first()
        return user is not None

    @staticmethod
    def active_user_with_email(email):
        """
        Active the user with given email
        """
        user = User.query.filter_by(email=email).first()
        if user and not user.active:
            user.active = True
            db.session.commit()

class Activation(db.Model):
    """
    Model for the activation table
    """
    id = db.Column(db.Integer, primary_key=True)
    user_email = db.Column(db.String(80))
    activation_code = db.Column(db.String(128))

    @staticmethod
    def get_activation(user_email, activation_code):
        activation = Activation.query.filter_by(user_email=user_email, activation_code=activation_code).first()
        return activation

    @staticmethod
    def create_activation(user_email, activation_code):
        activation = Activation()
        activation.user_email = user_email
        activation.activation_code = activation_code
        db.session.add(activation)
        db.session.commit()

class ContactContent(db.Model):
    """
    Model for the contact_content table
    Save information for a contact attempt
    """
    id = db.Column(db.Integer, primary_key=True)
    from_email = db.Column(db.String(80))
    question_type = db.Column(db.String(80))
    content =db.Column(db.String(1024))

    def __init__(self, email, question_type, content):
        self.from_email = email
        self.question_type = question_type
        self.content = content

class DeactiveRecord(db.Model):
    """
    Model for the deactive_record table
    Save information for deactive account
    """
    id = db.Column(db.Integer, primary_key=True)
    user_email = db.Column(db.String(80), unique=True)
    reason = db.Column(db.String(1024))
    time = db.Column(db.DateTime)

    def __init__(self, user_email, reason, time):
        self.user_email = user_email
        self.reason = reason
        self.time = time

class Question(db.Model):
    """
    Class for the question model
    Each user can have multiple questions
    """
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(1024))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    time = db.Column(db.DateTime)
    answers = db.relationship('Answer', backref='question', lazy='dynamic')

    def __init__(self, user_id, content, time):
        self.content = content
        self.user_id = user_id
        self.time = time

class Answer(db.Model):
    """
    Class for the answer model
    Each question can have multiple answers
    Each user can have multiple answers for multiple questions
    """
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(1024))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'))
    time = db.Column(db.DateTime)

    def __init__(self, question_id, user_id, content, time):
        self.question_id = question_id
        self.user_id = user_id
        self.content = content
        self.time = time

    @staticmethod
    def already_answered(question_id, user_id):
        """
        Check if a question was already answered by a given user
        """
        answer = Answer.query.filter_by(question_id=question_id, user_id=user_id).first()
        return answer is not None
